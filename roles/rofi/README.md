# rofi

Role to install (and optionally configure) `rofi` via `apt`

## Example

By default, no configuration of the role should be necessary for installation.

```yaml
...
roles:
- role: rofi
...
```

Adding paths to `rofi_config_files` will create symlinks in `rofi_config_root` which defaults to an appropriate location in the given `rofi_user` home directory.

```yaml
...
- role: rofi
  vars:
    rofi_config_files:
    - /example/path/to/config.rasi
...
```

## Role Variables

```yaml
# The user for which to copy configurations, if included; default to user running the playbook
# Options: any user with a home directory
rofi_user: '{{ ansible_env.SUDO_USER | default(ansible_env.USER) }}'

# Path to the rofi config files to use; default to none as it is optional
rofi_config_files: []

# Directory of links to above rofi config; default to appropriate location in `rofi_user` home
rofi_config_root: '/home/{{ rofi_user }}/.config/rofi'

# Directly affect the `force` option on the `ansible.builtin.file` module for links
# > force the creation of the symlinks in two cases: the source file does not exist (but will
# > appear later); the destination exists and is a file (so, we need to unlink the "path" file
# > and create symlink to the "src" file in place of it
# src: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html
rofi_config_link_force: no
```
