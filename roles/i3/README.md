# i3

Role to install (and optionally configure) `i3` via `apt`

## Example

By default, no configuration of the role should be necessary for installation.

```yaml
...
roles:
- role: i3
...
```

Adding paths to `i3_config_files` will create symlinks in `i3_config_root` which defaults to an appropriate location in the given `i3_user` home directory.

```yaml
...
- role: i3
  vars:
    i3_config_files:
    - /example/path/to/config
...
```

## Role Variables

```yaml
# The user for which to copy configurations, if included; default to user running the playbook
# Options: any user with a home directory
i3_user: '{{ ansible_env.SUDO_USER | default(ansible_env.USER) }}'

# Path to the i3 config files to use; default to none as it is optional
i3_config_files: []

# Directory of links to above i3 config; default to appropriate location in `i3_user` home
i3_config_root: '/home/{{ i3_user }}/.config/i3'

# Directly affect the `force` option on the `ansible.builtin.file` module for links
# > force the creation of the symlinks in two cases: the source file does not exist (but will
# > appear later); the destination exists and is a file (so, we need to unlink the "path" file
# > and create symlink to the "src" file in place of it
# src: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html
i3_config_link_force: no
```
