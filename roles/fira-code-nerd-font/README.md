# Fira Code Nerd Fonts

Role to install Fira Code Nerd Fonts

## Example

This role downloads all of the available faces of the Fira Code Nerd Font and refreshes the font cache without any additional configuration

```yaml
...
roles:
- role: fira-code-nerd-fonts
...
```

## Role Variables

```yaml
---
# Directory in which to install the downloaded fonts
# Used for simplicity of the below font list
# Options: any directory from which fonts are sourced
firacode_nerdfonts_dir: "/usr/share/fonts/truetype/firacode-nf"

# Repository and directory from which to download the fonts
# Used for simplicity of the below font list
firacode_nerdfonts_url: "https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/FiraCode"

# List of full URLs for each of the fonts to download and the names of the files once downloaded
# defaults/main.yml includes all of the available default fonts for Fira Code from the above repo
# Options: Default list can be shortened or modified to use the mono version of the fonts
firacode_nerdfonts:
- { url: "", dest: "" }
```
