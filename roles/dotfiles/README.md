# Dotfiles

Role to link a collection dotfiles to a centralized location

## Example

This role is built to work on a `dotfiles_src` that is structured like the intended destination. The `dotfiles_files` must be relative paths from `dotfiles_src` and links will be created relative to the `dotfiles_home` directory.

- `dotfiles_user` value: `exampleuser`
- default `dotfiles_home` expansion: `/home/exampleuser`
- default `dotfiles_src` expansion: `/home/exampleuser/.dotfiles`
- Absolute path of config: `/home/exampleuser/.dotfiles/.config/foo/bar.config`
- `dotfiles_files` entry for config: `.config/foo/bar.config`
- Link created: `/home/exampleuser/.config/foo/bar.config`

An implementation of this example using the default values would look like:

```yaml
...
- role: dotfiles
  vars:
    dotfiles_user: exampleuser
    dotfiles_files:
    - .config/foo/bar.config
...
```

## Role Variables

```yaml
# The user for which to copy configurations, if included; default to user running the playbook
# Options: any user with a home directory
dotfiles_user: '{{ ansible_env.SUDO_USER | default(ansible_env.USER) }}'

# Control whether or not the dotfiles repo will be cloned during this run
# Note: this is intended for initial setup, as it will fail if changes are present
dotfiles_clone: no

# Repository from which to clone if `dotfiles_clone` is true
dotfiles_repo: 'https://github.com/{{ dotfiles_user }}/dotfiles'

# The location where links will be created; default to user home
dotfiles_home: '/home/{{ dotfiles_user }}'

# Centralized source of dotfiles, such as a dotfiles git repo
# Also, destination of the cloned repo if `dotfiles_clone` is true
dotfiles_src: '/home/{{ dotfiles_user }}/.dotfiles'

# Relative path from `dotfiles_src` to the files for which to create link in `dotfiles_home`
dotfiles_files: []

# Directly affect the `force` option on the `ansible.builtin.file` module for links
# > force the creation of the symlinks in two cases: the source file does not exist (but will
# > appear later); the destination exists and is a file (so, we need to unlink the "path" file
# > and create symlink to the "src" file in place of it
# src: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html
dotfiles_link_force: no
```
