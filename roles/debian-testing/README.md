# Debian Testing

Role to disable release-named (e.g. `buster`) and enable Debian `testing` repositories

## Requirements

- Debian

## Example

For a fresh Debian installation with the default mirror and no additional sources, no configuration of the role should be necessary.

```yaml
...
roles:
  - role: debian-testing
...
```

For other cases, the role is flexible and can be configured with the variables below.

## Role Variables

```yaml
# The for the existing sources.list entries mirror, likely selected at installation
# Other options: Any debian mirror
debian_testing_default_mirror: http://deb.debian.org/debian/

# The mirror for security updates
# Other options: http://debian-archive.trafficmanager.net/debian-security/ (Azure)
debian_testing_security_url: http://security.debian.org/debian-security/

# The mirror for the testing entries to be added to sources.list
# Other options: Any debian mirror
debian_testing_selected_mirror: http://deb.debian.org/debian/

# Release-named repositories to remove, based on above mirror and host facts
# Other options: Whatever you have configured; default is the best guess for a new installation
debian_testing_default_repositories:
- 'deb {{ debian_testing_default_mirror }} {{ ansible_distribution_release }} main'
- 'deb-src {{ debian_testing_default_mirror }} {{ ansible_distribution_release }} main'
- 'deb {{ debian_testing_security_url }} {{ ansible_distribution_release }}-security main'
- 'deb-src {{ debian_testing_security_url }} {{ ansible_distribution_release }}-security main'

# Testing repositories to add, based on above mirror
# Other options: Whatever you need to configure; default is the best guess for a new installation
debian_testing_repositories:
- 'deb {{ debian_testing_selected_mirror }} testing main contrib'
- 'deb-src {{ debian_testing_selected_mirror }} testing main contrib'
- 'deb {{ debian_testing_security_url }} testing-security main contrib'
- 'deb-src {{ debian_testing_security_url }} testing-security main contrib'
```
