# i3blocks

Role to install (and optionally configure) `i3blocks` via `apt`

## Example

By default, no configuration of the role should be necessary for installation.

```yaml
...
roles:
- role: i3blocks
...
```

Adding paths to `i3blocks_config_files` will create symlinks in `i3blocks_config_root` which defaults to an appropriate location in the given `i3blocks_user` home directory. This may be useful for linking a directory (or directories) of custom blocklets

```yaml
...
- role: i3blocks
  vars:
    i3blocks_config_files:
    - /example/path/to/i3blocks.conf
    - /example/path/to/custom-blocks-dir
...
```

## Role Variables

```yaml
# The user for which to copy configurations, if included; default to user running the playbook
# Options: any user with a home directory
i3blocks_user: '{{ ansible_env.SUDO_USER | default(ansible_env.USER) }}'

# Whether or not to install (clone) the i3blocks/contrib repo of community blocklets
i3blocks_contrib_install: no

# Directory for the installation of the i3blocks-contrib repo
i3blocks_contrib_dir: '{{ i3blocks_config_root }}/contrib'

# Path to the i3blocks config files to use; default to none as it is optional
i3blocks_config_files: []

# Directory of links to above i3blocks config; default to appropriate location in `i3blocks_user` home
i3blocks_config_root: '/home/{{ i3blocks_user }}/.config/i3blocks'

# Directly affect the `force` option on the `ansible.builtin.file` module for links
# > force the creation of the symlinks in two cases: the source file does not exist (but will
# > appear later); the destination exists and is a file (so, we need to unlink the "path" file
# > and create symlink to the "src" file in place of it
# src: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html
i3blocks_config_link_force: no
```
