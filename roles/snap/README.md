# Snapd

Role to install `snapd` via `apt` and optionally install a set of snaps

## Example

By default, no configuration of the role should be necessary for installation.

```yaml
...
roles:
- role: snap
...
```

Specifying snaps in `snap_snaps` and/or `snap_classic_snaps`

```yaml
...
- role: snap
  vars:
    snap_snaps:
    - snap_one
    - snap_two
    snap_classic_snaps:
    - classic_snap_one
    - classic_snap_two
...
```

## Role Variables

```yaml
# List of snaps to install
snap_snaps: []

# Directly affect the `classic` option, representing the snap flag `--classic`
# > Confinement policy. The classic confinement allows a snap to have the same level of
# > access to the system as "classic" packages, like those managed by APT. This option
# > corresponds to the --classic argument. This option can only be specified if there
# > is a single snap in the task.
# src: https://docs.ansible.com/ansible/latest/collections/community/general/snap_module.html
snap_classic_snaps: []
```
