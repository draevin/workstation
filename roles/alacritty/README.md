# Alacritty

Role to install (and optionally configure) [alacritty](https://github.com/alacritty/alacritty) on Debian and Ubuntu

## Requirements

- Debian or Ubuntu

## Example

For Ubuntu, no configuration of the role should be necessary.

```yaml
...
roles:
- role: alacritty
...
```

For Debian, the `alacritty_ppa_codename` can be used to override the PPA codename to an appropriate value. This must be a version of Ubuntu that is based on the version of Debian being used. For Debian `testing` at the time of writing, this would be `focal` and could be implemented in the playbook as follows:

```yaml
...
roles:
- role: alacritty
  vars:
    alacritty_ppa_codename: focal
...
```

Adding `alacritty_config_src` with a path to an alacritty configuration file will create a symlink from `alacritty_config_dest` which defaults to an appropriate location in the given `alacritty_user` home directory, but can be modified to any of the [alacritty config locations](https://github.com/alacritty/alacritty#configuration).

```yaml
...
- role: alacritty
  vars:
    alacritty_config_src: /example/path/to/alacritty.yml
...
```

## Role Variables

```yaml
# Used to override the PPA codename for Debian; default using host facts for Ubuntu
# Options: an Ubuntu version codename for a version based on the installed version of Debian
alacritty_ppa_codename: "{{ ansible_distribution_release }}"

# The user for which to copy a configuration, if included; default to user running the playbook
# Options: any user with a home directory
alacritty_user: '{{ ansible_env.SUDO_USER | default(ansible_env.USER) }}'

# Path to the alacritty config file to use; default to none as it is optional
alacritty_config_src:

# Path of link to above alacritty config; default to default alacritty.yml location
alacritty_config_dest: '/home/{{ alacritty_user }}/.config/alacritty/alacritty.yml'
```
