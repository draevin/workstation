# Powerlevel10k (w/ ZSH and Oh My ZSH)

Role to install (and optionally configure) `zsh` with [Oh My ZSH](https://github.com/ohmyzsh/eohmyzsh) and, ultimately, the [Powerlevel10k](https://github.com/romkatv/powerlevel10k) theme via `apt` and `git`

## Example

By default, no configuration of the role should be necessary for installation.

```yaml
...
roles:
- role: p10k
...
```

Adding paths to `zsh_config_files` will create symlinks in `zsh_config_root` which defaults to the given `zsh_user` home directory.

```yaml
...
- role: zsh
  vars:
    zsh_config_files:
    - /example/path/to/.zshrc
    - /example/path/to/.zprofile
...
```

## Role Variables

```yaml
# The user for which to copy configurations, if included; default to user running the playbook
# Options: any user with a home directory
zsh_user: '{{ ansible_env.SUDO_USER | default(ansible_env.USER) }}'

# Path to the zsh config files to use; default to none as it is optional
zsh_config_files: []

# Directory of links to above zsh config; default to `zsh_user` home
zsh_config_root: '/home/{{ zsh_user }}/.config/.gnupg'

# Directly affect the `force` option on the `ansible.builtin.file` module for links
# > force the creation of the symlinks in two cases: the source file does not exist (but will
# > appear later); the destination exists and is a file (so, we need to unlink the "path" file
# > and create symlink to the "src" file in place of it
# src: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html
zsh_config_link_force: no
```
