# /bin/sh

# sudo ./setup.sh {{desktop|laptop}}

WORKSTATION=$1

set -eux

WORKSTATION_DIR=$(dirname $(readlink -f $0))

apt-get install ansible -y
ansible-galaxy collection install community.general
ansible-galaxy install -r ./roles/galaxy/requirements.yml -p ./roles/galaxy -e
chown -R $(logname):$(logname) $WORKSTATION_DIR/roles/galaxy
ansible-playbook $WORKSTATION_DIR/$WORKSTATION.yml -e dotfiles_clone=true
su $(logname) -c "wget -O- -q https://cdn.draevin.com/public.asc | gpg2 --import -"