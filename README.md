# Workstation Setup

Base setup for my workstations, based on a minimal Debian (Testing) installation, using Ansible wherever possible.

## Preface

This setup is fully opinionated and gets me what I need/want for daily use of my personal workstations. I would advise against any unmodified use of it by other parties. It is public for two reasons:

1. Fresh installs don't have keys to private repos, and
2. It serves as a codified version of my [drae.vin/uses](https://drae.vin/uses) page.

I'm always open to questions or recommendations on the tools or methods I use, so feel free to drop an issue or get ahold of me [elsewhere](https://drae.vin/#contacts).

## Usage

### Pre-steps

1. Install from [Debian Testing net installation image](https://cdimage.debian.org/cdimage/weekly-builds/)
   1. Partition appropriately for the given device
   2. Uncheck all software except `System Utilities` when prompted
2. Follow the below prep command steps:

```shell
> su
  > apt install sudo git
  > /sbin/useradd -aG {{ username }} sudo
  > exit
```

### setup.sh

Clone this repository and run the included `setup.sh` script, specifying which machine is to be set up.

```shell
> mkdir src && cd src
> git clone https://gitlab.com/draevin/workstation
> cd workstation
> chmod o+x setup.sh
> sudo ./setup.sh {{desktop|laptop}}
```

### Follow-up Steps

The key should have been obtained through `setup.sh` but needs to manually trusted for use.

```shell
> export KEYID=$(gpg2 --list-key | grep ^pub | sed "s/pub   rsa4096\/0x//" | cut -c1-16)
> gpg2 --edit-key $KEYID
  > trust
    > 5 #for ultimate trust
    > y
  > quit
# remove and reinsert Yubikey
# log out and log back in
```

## Roles Included

- [alacritty](roles/alacritty/README.md) - Install alacritty with optional configuration
- [debian-testing](roles/debian-testing/README.md) - Set up Debian `testing` repos
- [dotfiles](roles/dotfiles/README.md) - Link additional dotfiles from a centralized location
- [fira-code-nerd-font](roles/fira-code-nerd-font/README.md) - Install Fira Code Nerd Fonts
- [gpg](roles/gpg/README.md) - Install gpg and gpg-agent with optional configuration
- [i3](roles/i3/README.md) - Install i3 with optional configuration
- [i3blocks](roles/i3blocks/README.md) - Install i3blocks with optional configuration + `i3blocks-contrib`
- [p10k](roles/p10k/README.md) - Install ZSH + Oh My ZSH and with Powerlevel10k theme and optional configuration
- [snap](roles/snap/README.md) - Install snapd with an optional list of snaps

This playbook also makes use of some Ansible Galaxy roles which can be found in the [requirements](roles/galaxy/requirements.yml) file and are installed to the `roles/galaxy` directory
